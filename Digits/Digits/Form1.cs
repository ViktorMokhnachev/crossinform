﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Digits
{
    public partial class Form1 : Form
    {
        //таблица с цифрами
        private int[,] digits = new int[3, 3];
        private TextBox[,] textBoxes = new TextBox[3, 3];
        //доступные (ещё не использованные) цифры
        private List<int> availableDigits = new List<int>();

        public Form1()
        {
            InitializeComponent();
            textBoxes[0, 0] = Text00;
            textBoxes[0, 1] = Text01;
            textBoxes[0, 2] = Text02;
            textBoxes[1, 0] = Text10;
            textBoxes[1, 1] = Text11;
            textBoxes[1, 2] = Text12;
            textBoxes[2, 0] = Text20;
            textBoxes[2, 1] = Text21;
            textBoxes[2, 2] = Text22;
        }
        
        private void BtnGenerateDigits_Click(object sender, EventArgs e)
        {
            //Генерация таблицы с цифрами и вывод на форму
            List<int> availableDigits = new List<int>(9);
            digits = new int[3, 3];
            Random random = new Random();
            for (int i = 1; i <= 9; i++)
            {
                availableDigits.Add(i);
            }
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    int r;
                    do r = random.Next(9) + 1;
                    while (!availableDigits.Contains(r));
                    textBoxes[i, j].Text = r.ToString();
                    digits[i, j] = r;
                    availableDigits.Remove(r);
                }
            }
            BtnAnswer.Enabled = true;
        }

        private void BtnAnswer_Click(object sender, EventArgs e)
        {
            availableDigits = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            //первая цифра числа должна находиться в углу или в центре
            //иначе в результате получится только восьмизначное число
            //поэтому ищем максимальную цифру из тех, что находятся по углам и в центре
            Dictionary<int, string> firstDigitCandidates = new Dictionary<int, string>(5);
            firstDigitCandidates.Add(digits[0, 0], "00");
            firstDigitCandidates.Add(digits[0, 2], "02");
            firstDigitCandidates.Add(digits[1, 1], "11");
            firstDigitCandidates.Add(digits[2, 0], "20");
            firstDigitCandidates.Add(digits[2, 2], "22");
            //первая цифра числа
            int firstDigit = firstDigitCandidates.Keys.Max();
            //её координаты в таблице
            int x = Convert.ToInt32(firstDigitCandidates[firstDigit].Substring(0, 1));
            int y = Convert.ToInt32(firstDigitCandidates[firstDigit].Substring(1, 1));
            //убираем первую цифру из доступных
            availableDigits.Remove(firstDigit);
            //далее смотрим соседей этой цифры и выбираем максимальную, затем смотрим соседей выбранной цифры и т.д.
            //и тем самым составляем ответ
            string answer = firstDigit + GetNearbyDigits(firstDigit, x, y);
            //выводим ответ на форму
            TextAnswer.Text = answer;
        }

        /// <summary>
        /// Получить соседей цифры в таблице и выбрать наиболее подходящую
        /// </summary>
        /// <param name="a">Цифра, соседей которой ищем</param>
        /// <param name="x">Координата х цифры а в таблице</param>
        /// <param name="y">Координата у цифры а в таблице</param>
        /// <returns>Часть ответа от переданной цифры до конца числа</returns>
        private string GetNearbyDigits(int a, int x, int y)
        {
            //перечень соседей цифры и их координат
            Dictionary<int, int[,]> nearbyDigits = new Dictionary<int, int[,]>();
            //если сверху от цифры есть доступная цифра
            if (x - 1 >= 0 && availableDigits.Contains(digits[x - 1, y]))
            {
                //запоминаем координаты соседа сверху
                int[,] coord = { { x - 1 }, { y } };
                //добавляем в перечень соседей
                nearbyDigits.Add(digits[x - 1, y], coord);
            }
            //если снизу от цифры есть доступная цифра
            if (x + 1 < 3 && availableDigits.Contains(digits[x + 1, y]))
            {
                //запоминаем координаты соседа снизу
                int[,] coord = { { x + 1 }, { y } };
                //добавляем в перечень соседей
                nearbyDigits.Add(digits[x + 1, y], coord);
            }
            //если слева от цифры есть доступная цифра
            if (y - 1 >= 0 && availableDigits.Contains(digits[x, y - 1]))
            {
                //запоминаем координаты соседа слева
                int[,] coord = { { x }, { y - 1 } };
                //добавляем в перечень соседей
                nearbyDigits.Add(digits[x, y - 1], coord);
            }
            //если справа от цифры есть доступная цифра
            if (y + 1 < 3 && availableDigits.Contains(digits[x, y + 1]))
            {
                //запоминаем координаты соседа справа
                int[,] coord = { { x }, { y + 1 } };
                //добавляем в перечень соседей
                nearbyDigits.Add(digits[x, y + 1], coord);
            }
            //если доступных цифр по соседству нет - попали в тупик и нужно вернуться назад
            if (nearbyDigits.Count == 0)
            {
                return null;
            }
            //количество соседей
            int k = nearbyDigits.Keys.Count;
            //цикл по всем соседям
            for (int j = 0; j < k; j++)
            {
                //выбираем максимальную цифру из соседей
                int n = nearbyDigits.Keys.Max();
                //убираем её из перечня доступных (предполагаем, что она сейчас будет добавлена к ответу)
                availableDigits.Remove(n);
                //рекурсивно проверяем, подходит ли нам эта цифра (не попадём ли мы в тупик раньше времени)
                string answer = GetNearbyDigits(n, nearbyDigits[n][0, 0], nearbyDigits[n][1, 0]);
                //если попали в тупик, но не все цифры ещё использованы для ответа
                if ((answer == null || !answer.Contains(n.ToString())) && availableDigits.Count > 0)
                {
                    //убираем проверенную цифру из числа соседей переданной
                    nearbyDigits.Remove(n);
                    //возвращаем её в перечень доступных цифр
                    availableDigits.Add(n);
                }
                //если цифра нам подошла или все доступные для ответа цифры кончились
                else
                {
                    //возвращаем ответ
                    return n + answer;
                }
            }
            //попали в тупик
            return null;
        }

        /// <summary>
        /// Получить координаты цифры в таблице
        /// </summary>
        /// <param name="n">Цифра, для которой требуется определить координаты</param>
        /// <returns>Массив с координатами, где элемент 0 - координата х, элемент 1 - координата у</returns>
        private int[] GetCoordinates(int n)
        {
            int[] coords = new int[2];
            for (int i = 0; i < digits.GetLength(0); i++)
            {
                for (int j = 0; j < digits.GetLength(1); j++)
                {
                    if (digits[i, j] == n)
                    {
                        coords[0] = i;
                        coords[1] = j;
                    }
                }
            }
            return coords;
        }
    }
}
