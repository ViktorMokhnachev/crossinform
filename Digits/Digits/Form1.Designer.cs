﻿namespace Digits
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Text22 = new System.Windows.Forms.TextBox();
            this.Text21 = new System.Windows.Forms.TextBox();
            this.Text20 = new System.Windows.Forms.TextBox();
            this.Text12 = new System.Windows.Forms.TextBox();
            this.Text11 = new System.Windows.Forms.TextBox();
            this.Text10 = new System.Windows.Forms.TextBox();
            this.Text02 = new System.Windows.Forms.TextBox();
            this.Text01 = new System.Windows.Forms.TextBox();
            this.Text00 = new System.Windows.Forms.TextBox();
            this.BtnGenerateDigits = new System.Windows.Forms.Button();
            this.BtnAnswer = new System.Windows.Forms.Button();
            this.TextAnswer = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Text22);
            this.groupBox1.Controls.Add(this.Text21);
            this.groupBox1.Controls.Add(this.Text20);
            this.groupBox1.Controls.Add(this.Text12);
            this.groupBox1.Controls.Add(this.Text11);
            this.groupBox1.Controls.Add(this.Text10);
            this.groupBox1.Controls.Add(this.Text02);
            this.groupBox1.Controls.Add(this.Text01);
            this.groupBox1.Controls.Add(this.Text00);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(131, 130);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Исходные данные";
            // 
            // Text22
            // 
            this.Text22.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Text22.Location = new System.Drawing.Point(88, 91);
            this.Text22.Name = "Text22";
            this.Text22.ReadOnly = true;
            this.Text22.Size = new System.Drawing.Size(35, 30);
            this.Text22.TabIndex = 8;
            this.Text22.Text = "0";
            this.Text22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Text21
            // 
            this.Text21.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Text21.Location = new System.Drawing.Point(47, 91);
            this.Text21.Name = "Text21";
            this.Text21.ReadOnly = true;
            this.Text21.Size = new System.Drawing.Size(35, 30);
            this.Text21.TabIndex = 7;
            this.Text21.Text = "0";
            this.Text21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Text20
            // 
            this.Text20.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Text20.Location = new System.Drawing.Point(6, 91);
            this.Text20.Name = "Text20";
            this.Text20.ReadOnly = true;
            this.Text20.Size = new System.Drawing.Size(35, 30);
            this.Text20.TabIndex = 6;
            this.Text20.Text = "0";
            this.Text20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Text12
            // 
            this.Text12.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Text12.Location = new System.Drawing.Point(88, 55);
            this.Text12.Name = "Text12";
            this.Text12.ReadOnly = true;
            this.Text12.Size = new System.Drawing.Size(35, 30);
            this.Text12.TabIndex = 5;
            this.Text12.Text = "0";
            this.Text12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Text11
            // 
            this.Text11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Text11.Location = new System.Drawing.Point(47, 55);
            this.Text11.Name = "Text11";
            this.Text11.ReadOnly = true;
            this.Text11.Size = new System.Drawing.Size(35, 30);
            this.Text11.TabIndex = 4;
            this.Text11.Text = "0";
            this.Text11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Text10
            // 
            this.Text10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Text10.Location = new System.Drawing.Point(6, 55);
            this.Text10.Name = "Text10";
            this.Text10.ReadOnly = true;
            this.Text10.Size = new System.Drawing.Size(35, 30);
            this.Text10.TabIndex = 3;
            this.Text10.Text = "0";
            this.Text10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Text02
            // 
            this.Text02.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Text02.Location = new System.Drawing.Point(88, 19);
            this.Text02.Name = "Text02";
            this.Text02.ReadOnly = true;
            this.Text02.Size = new System.Drawing.Size(35, 30);
            this.Text02.TabIndex = 2;
            this.Text02.Text = "0";
            this.Text02.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Text01
            // 
            this.Text01.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Text01.Location = new System.Drawing.Point(47, 19);
            this.Text01.Name = "Text01";
            this.Text01.ReadOnly = true;
            this.Text01.Size = new System.Drawing.Size(35, 30);
            this.Text01.TabIndex = 1;
            this.Text01.Text = "0";
            this.Text01.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Text00
            // 
            this.Text00.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Text00.Location = new System.Drawing.Point(6, 19);
            this.Text00.Name = "Text00";
            this.Text00.ReadOnly = true;
            this.Text00.Size = new System.Drawing.Size(35, 30);
            this.Text00.TabIndex = 0;
            this.Text00.Text = "0";
            this.Text00.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BtnGenerateDigits
            // 
            this.BtnGenerateDigits.Location = new System.Drawing.Point(149, 31);
            this.BtnGenerateDigits.Name = "BtnGenerateDigits";
            this.BtnGenerateDigits.Size = new System.Drawing.Size(155, 30);
            this.BtnGenerateDigits.TabIndex = 1;
            this.BtnGenerateDigits.Text = "Сгенерировать таблицу";
            this.BtnGenerateDigits.UseVisualStyleBackColor = true;
            this.BtnGenerateDigits.Click += new System.EventHandler(this.BtnGenerateDigits_Click);
            // 
            // BtnAnswer
            // 
            this.BtnAnswer.Enabled = false;
            this.BtnAnswer.Location = new System.Drawing.Point(149, 67);
            this.BtnAnswer.Name = "BtnAnswer";
            this.BtnAnswer.Size = new System.Drawing.Size(155, 30);
            this.BtnAnswer.TabIndex = 2;
            this.BtnAnswer.Text = "Сформировать ответ";
            this.BtnAnswer.UseVisualStyleBackColor = true;
            this.BtnAnswer.Click += new System.EventHandler(this.BtnAnswer_Click);
            // 
            // TextAnswer
            // 
            this.TextAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextAnswer.Location = new System.Drawing.Point(149, 103);
            this.TextAnswer.Name = "TextAnswer";
            this.TextAnswer.ReadOnly = true;
            this.TextAnswer.Size = new System.Drawing.Size(155, 30);
            this.TextAnswer.TabIndex = 9;
            this.TextAnswer.Text = "0123456789";
            this.TextAnswer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 152);
            this.Controls.Add(this.TextAnswer);
            this.Controls.Add(this.BtnAnswer);
            this.Controls.Add(this.BtnGenerateDigits);
            this.Controls.Add(this.groupBox1);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(335, 190);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(335, 190);
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox Text22;
        private System.Windows.Forms.TextBox Text21;
        private System.Windows.Forms.TextBox Text20;
        private System.Windows.Forms.TextBox Text12;
        private System.Windows.Forms.TextBox Text11;
        private System.Windows.Forms.TextBox Text10;
        private System.Windows.Forms.TextBox Text02;
        private System.Windows.Forms.TextBox Text01;
        private System.Windows.Forms.TextBox Text00;
        private System.Windows.Forms.Button BtnGenerateDigits;
        private System.Windows.Forms.Button BtnAnswer;
        private System.Windows.Forms.TextBox TextAnswer;
    }
}

