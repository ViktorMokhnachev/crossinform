﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Triplets
{
    class Program
    {
        static void Main(string[] args)
        {
            //сигнал отмены для токена
            CancellationTokenSource cancelTokenSource = new CancellationTokenSource();
            //токен отмены, распространяющий сигнал
            CancellationToken token = cancelTokenSource.Token;
            //сообщение в консоль
            Console.WriteLine("Введите строку со словами через запятую.");
            //исходная введённая строка
            string str = Console.ReadLine();
            //поток, обрабатывающий ввод команды отмены
            Thread cancel = new Thread(() =>
            {
                Console.WriteLine("Введите Y для отмены операции.");
                string s = Console.ReadLine();
                //чтобы отменить операцию, нужно ввести "Y" или "y" и нажать Enter
                if (s == "Y" || s == "y")
                    cancelTokenSource.Cancel();
                Console.ReadKey();
            });
            //поток, выполняющий поиск триплетов во введённой строке
            Thread triplets = new Thread(() =>
            {
                string result = MostFrequentTriplet(str, token);
                //если процесс не был прерван, выводит результат и завершает поток, обрабатывающий ввод команды отмены
                if (result != null)
                {
                    Console.WriteLine("Самые частые триплеты: {0}", result);
                    cancel.Abort();
                    //выход из программы по нажатию любой клавиши
                    Console.ReadKey();
                }
            });
            //запуск потока поиска триплетов
            triplets.Start();
            //запуск потока обработки команды отмены
            cancel.Start();
            //прикрепляем к основному потоку, чтобы выход из программы не произошёл раньше положенного
            cancel.Join();
        }

        /// <summary>
        /// Метод выполняет поиск триплетов в переданной строке.
        /// </summary>
        /// <param name="str">Входная строка, в которой будет выполняться поиск триплетов.
        /// Предплоагается, что она содержит слова, разделённые запятой.</param>
        /// <param name="token">Токен отмены операции.</param>
        /// <returns>Строка с перечнем самых частых триплетов и частотой их появления.</returns>
        private static string MostFrequentTriplet(string str, CancellationToken token)
        {
            //переводим переданную строку в нижний регистр и разбиваем на массив строк по запятой
            string[] words = str.ToLower().Split(',');
            //переменная с результатом
            string result = "";
            //потокобезопасная коллекия вида "ключ - значение", где ключ - триплет, значение - количество его вхождений
            ConcurrentDictionary<string, int> triplets = new ConcurrentDictionary<string, int>();
            //для каждого слова создаём новый поток
            foreach (string word in words)
            {
                //если поступил сигнал отмены, прекращаем выполнение
                if (token.IsCancellationRequested)
                {
                    Console.WriteLine("Операция прервана.");
                    return null;
                }
                Thread thread = new Thread(() =>
                {
                    //раскомментировать для проверки отмены
                    //Thread.Sleep(2000);
                    //разбиваем слово на символы
                    char[] chars = word.ToCharArray();
                    //поиск триплетов в слове
                    for (int i = 0; i < chars.Count() - 2; i++)
                    {
                        string triplet = "" + chars[i] + chars[i + 1] + chars[i + 2];
                        //если коллекция уже содержит найденный триплет, увеличиваем его значение на 1
                        if (triplets.ContainsKey(triplet))
                        {
                            int c = triplets[triplet];
                            int d = c + 1;
                            triplets.TryUpdate(triplet, d, c);
                        }
                        //иначе добавляем новый триплет в коллекцию
                        else
                        {
                            triplets.TryAdd(triplet, 1);
                        }
                    }
                });
                //запускаем поток и присоединяем его к основному
                thread.Start();
                thread.Join();
            }
            //после завершения всех потоков
            if (!triplets.IsEmpty)
            {
                //ищем максимальное значение числа вхождений триплетов
                int max = triplets.Values.Max();
                //получаем индекс первого и последнего элемента в коллекции, равного максимальному
                int first = triplets.Values.ToList().IndexOf(max);
                int last = triplets.Values.ToList().LastIndexOf(max);
                //если индексы равны, полагаем, что наиболее часто встречающийся триплет всего один,
                //иначе
                if (first != last)
                {
                    //создаём список, в который будем класть все наиболее часто встречающиеся триплеты
                    List<string> resTriplets = new List<string>();
                    for (int i = first; i < last; i++)
                    {
                        if (triplets.Values.ToList()[i] == max)
                        {
                            //кладём
                            resTriplets.Add(triplets.Keys.ToList()[i]);
                        }
                    }
                    //формируем результирующую строку, в которую выводим все наиболее часто встречающиеся триплеты
                    //через запятую и количество их вхождений
                    result = string.Format("{0}\t{1}", string.Join(",", resTriplets), max);
                }
                else
                {
                    //формируем результирующую строку, в которую выводим наиболее часто встречающийся триплет
                    //и количество его вхождений
                    result = string.Format("{0}\t{1}", triplets.Keys.ToList()[first], max);
                }
                return result;
            }
            return null;
        }
    }
}
